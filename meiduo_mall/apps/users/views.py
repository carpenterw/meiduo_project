from django.shortcuts import render
from django.views import View
from django import http
from apps.users.models import User
from django_redis import get_redis_connection
from django.contrib.auth import login, logout
# 使用django自身方法实现用户登录
from django.contrib.auth import authenticate
import json
import re

# Create your views here.
class UsernameCountView(View):
    """
    用户名重复判断
    """
    def get(self, request, username):
        # 业务逻辑
        count = User.objects.filter(username=username).count()
        # 返回json数据
        return http.JsonResponse({'count': count})

class MobileCountView(View):
    """
    手机号码重复判断
    """
    def get(self, request, mobile):
        # 业务逻辑
        count = User.objects.filter(mobile=mobile).count()
        # 返回json数据
        return http.JsonResponse({'count': count})

class RegisterView(View):
    """
    用户注册
    """
    def post(self, request):
        # 1. 接收前端传递的json数据
        json_str = request.body.decode()
        # 2. 将json转化为字典数据
        # json_dict = json.loads(request.body.decode())
        json_dict = json.loads(json_str)

        # 3. 从字典中提取相关的字段数据
        username = json_dict.get('username')
        password = json_dict.get('password')
        password2 = json_dict.get('password2')
        mobile = json_dict.get('mobile')
        allow = json_dict.get('allow')
        sms_code = json_dict.get('sms_code')
        # 4. 验证字段数据
        if not all([username,password,password2,mobile,allow,sms_code]):
            return http.JsonResponse({'code':400,'errmsg': '缺少必传参数'})
        # 校验用户名格式
        if not re.match(r'^[a-zA-Z0-9_-]{5,20}$', username):
            return http.JsonResponse({'code': 400, 'errmsg': '用户名格式错误'})

        # 校验密码格式
        if not re.match(r'^[a-zA-Z0-9]{8,20}$', password):
            return http.JsonResponse({'code': 400, 'errmsg': '密码格式错误'})

        # 判断password2 和password是否相等
        if password2 != password:
            return http.JsonResponse({'code': 400, 'errmsg': '密码两次输入一致'})

        # mobile校验
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return http.JsonResponse({'code': 400, 'errmsg': 'mobile格式有误'})

        if not allow:
            return http.JsonResponse({'code': 400, 'errmsg': '未同意协议'})

        # 短信验证判断
        # 连接redis
        conn = get_redis_connection('verify')
        # 获取redis短信数据,bytes类型
        real_code = conn.get(f"sms_code_{mobile}")

        # 判断验证码是否过期
        if not real_code:
            return http.JsonResponse({'code': 400, 'errmsg': '验证码无效'})
        # 判断是否一致
        if sms_code != real_code.decode():
            return http.JsonResponse({'code': 400, 'errmsg': '验证码输入错误'})
        # 5. 将数据保存在数据库中
        user = User.objects.create_user(
            username=username,
            password=password,
            mobile=mobile
        )

        # 用户注册成功，需要状态保持
        login(request, user)
        # 6. 返回结果
        return http.JsonResponse({'code': 0, 'errmsg': 'ok'})

class LoginView(View):
    """
    用户登录
    """
    def post(self, request):
        # 接收数据
        json_data = request.body.decode()
        dict_data = json.loads(json_data)

        username = dict_data.get('username')
        password = dict_data.get('password')
        remembered = dict_data.get('remembered')

        # 校验数据
        if not all([username, password]):
            return http.JsonResponse({'code': 400, 'errmsg': '参数缺失'})

        # 验证用户是否存在
        # try:
        #     user = User.objects.get(username=username)
        # except:
        #     return http.JsonResponse({'code': 400, 'errmsg': '用户不存在'})
        #
        # if not user.check_password(password):
        #     return http.JsonResponse({'code': 400, 'errmsg': '密码不正确'})
        # 用手机号码作为用户名
        # user = User.objects.get(mobile=username)

        # 使用django自身方法实现判断用户是否存在
        user = authenticate(username=username, password=password)
        if not user:
            return http.JsonResponse({'code': 400, 'errmsg': '用户名或密码不正确'})
        # 状态保持
        login(request, user)
        # 返回结果
        response = http.JsonResponse({'code': 0, 'errmsg': 'ok'})
        if remembered:
            # 记住密码，设置session有效期为两周
            request.session.set_expiry(None)
            response.set_cookie('username',
                                user.username,
                                max_age=60 * 60 * 24 * 14)
        else:
            request.session.set_expiry(0)
            response.set_cookie('username', user.username)

        return response

class LogoutView(View):
    """
    退出登录
    """
    def delete(self, request):
        # logout方法实现退出登录，主要对session数据操作
        logout(request)
        # 登录时写cookie数据，退出时删除cookie数据
        response = http.JsonResponse({'code': 0, 'errmsg': 'ok'})
        response.delete_cookie('username')
        # 返回结果
        return response



