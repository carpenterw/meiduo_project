from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
# 自定义用户模型类
class User(AbstractUser):
    # 自定义手机号字段
    mobile = models.CharField(max_length=11, unique=True)

    class Meta:
        # 指定表名
        db_table = 'tb_user'
        verbose_name = '用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        # 当用户对象打印输出时显示指定返回结果
        return self.username