from django.urls import path
from . import views


urlpatterns = [
    path('image_codes/<uuid:uuid>/', views.ImageView.as_view()),
    path('sms_codes/<mobile:mobile>/', views.SMSCodeView.as_view())
]
