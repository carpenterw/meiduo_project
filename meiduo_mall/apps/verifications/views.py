from django.shortcuts import render
from django.views import View
from django import http
from apps.verifications.libs.captcha.captcha import captcha
from django_redis import get_redis_connection
from celery_tasks.sms_code.tasks import send_smscode
import random
from apps.verifications.libs.yuntongxun.ccp_sms import CCP

# Create your views here.
class ImageView(View):
    """
    图片验证码实现
    """
    def get(self, request, uuid):
        # 生成图片验证码
        text, img = captcha.generate_captcha()
        # 保存图片验证码
        conn = get_redis_connection('verify')
        conn.setex(f"img_{uuid}",300,text)
        return http.HttpResponse(img, content_type='image/jpg')



class SMSCodeView(View):
    """
    发送短信功能
    """
    def get(self, request, mobile):

        # 建立redis链接
        conn = get_redis_connection('verify')

        # 补充：频繁发送短信，判断两次之间的时间间隔
        flag_data = conn.get(f"flag_{mobile}")

        if flag_data:
            return http.JsonResponse({'code': 400, 'errmsg': '请求过于频繁'})

        # 1.获取前端数据
        image_code = request.GET.get('image_code')
        image_code_id = request.GET.get('image_code_id')

        if not all([image_code, image_code_id]):
            return http.JsonResponse({'errmsg':'参数不完整'})
        # 2.验证数据

        # 提取验证码数据
        real_smscode = conn.get(f"img_{image_code_id}")
        real_smscode = real_smscode.decode()
        # 比对数据
        if image_code.lower() != real_smscode.lower():
            return http.JsonResponse({'code': 400, 'errmsg': '验证码错误'})
        # 3.生成短信验证码
        sms_code = '%d'% random.randint(100000, 999999)
        print(sms_code)
        # 4.保存短信验证码
        # conn.setex(f"sms_code_{mobile}", 300, sms_code)
        #
        # # 增加判断时间间隔的标志
        # conn.setex(f"flag_{mobile}", 60, 'flag_data')
        # 5.发送短信
        # 使用管道写入数据
        # 生成管道对象
        pipe = conn.pipeline()
        # 使用管道写入redis命令
        pipe.setex(f"sms_code_{mobile}", 300, sms_code)
        pipe.setex(f"flag_{mobile}", 60, 'flag_data')
        # 执行管道命令
        pipe.execute()

        # ccp = CCP()
        # 模板序号为1
        # ccp.send_template_sms(mobile, [sms_code, 5], 1)

        # 使用celery异步调用短信发送任务
        send_smscode.delay(mobile, sms_code)
        # 6.返回结果
        return http.JsonResponse({'code': 400, 'errmsg': 'ok'})
