# 在文件中定义任务方法
from celery_tasks.yuntongxun.ccp_sms import CCP
from celery_tasks.main import celery_app

# 指定任务方法为send_smscode
@celery_app.task(name='send_smscode')
def send_smscode(mobile, sms_code):
    """
    发送短信
    :return:
    """
    ccp = CCP()
    # 模板序号为1
    ccp.send_template_sms(mobile, [sms_code, 5], 1)