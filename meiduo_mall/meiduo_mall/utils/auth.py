# 重写django的authenticate,支持手机号码查询
from django.contrib.auth.backends import ModelBackend
import re

from apps.users.models import User


class AuthModel(ModelBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        try:
            if re.match(r'1[3-9]\d{9}', username):
                user = User.objects.get(mobile=username)
            else:
                user = User.objects.get(username=username)
        except:
            user = None

        if user and user.check_password(password):
            return user
        return None