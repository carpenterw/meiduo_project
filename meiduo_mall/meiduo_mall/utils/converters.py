
# 自定义用户名匹配转换器
class UsernameConverter:
    # 编写正则匹配规则
    regex = '[a-zA-Z0-9_-]{5,20}'

    def to_python(self, value):
        # 视图中获取数据
        return str(value)

    def to_url(self, value):
        # 反向解析获取数据
        return str(value)

# 自定义手机号码匹配转换器
class MobileConverter:
    regex = '1[3-9]\d{9}'

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)